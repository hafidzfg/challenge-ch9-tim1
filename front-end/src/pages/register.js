import React, { Component } from "react";
import "../style/Register.css";
import Navbar from "../components/NavbarElement";

import {
  Container,
  Form,
  FormGroup,
  Button,
  Label,
  Input,
  FormFeedback,
} from "reactstrap";

export default class Register extends Component {
  state = {
    email: "",
    username: "",
    password: "",
    confirmPassword: "",
  };
  register() {
    fetch("http://localhost:4000/api/v1/register", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(this.state),
    }).then(console.log(this.state));
  }

  render() {
    return (
      <>
        <Navbar />{" "}
        <Container className="container-register">
          <Form className="register col-sm-6">
            <h2 className="title_register">REGISTER</h2>
            <FormGroup className="form-group">
              <Label for="email">Email</Label>
              <Input
                id="Email"
                name="email"
                placeholder="Write your email"
                type="email"
                onChange={(e) => this.setState({ email: e.target.value })}
              />
            </FormGroup>

            <FormGroup className="form-group">
              <Label for="username">Username</Label>
              <Input
                id="Username"
                name="username"
                placeholder="Create your own username"
                type="text"
                onChange={(e) => this.setState({ username: e.target.value })}
              />
            </FormGroup>

            <FormGroup className="form-group">
              <Label for="password">Password</Label>
              <Input
                id="Password"
                name="password"
                placeholder="Write your password"
                type="password"
                onChange={(e) => this.setState({ password: e.target.value })}
              />
            </FormGroup>

            <FormGroup className="form-group">
              <Label for="confirmPassword">Confirm Password</Label>
              <Input
                id="confirmPassword"
                name="password"
                placeholder="Confirm your password"
                type="password"
                onChange={(e) =>
                  this.setState({ confirmPassword: e.target.value })
                }
              />
            </FormGroup>

            <FormGroup className="button">
              <Button color="primary" onClick={() => this.register()}>
                Submit
              </Button>
            </FormGroup>
          </Form>
        </Container>
      </>
    );
  }
}
