import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col, Image } from "reactstrap";
import "../style/game-rps/GameRps.css";
import Play from "../components/game-rps/Play";

export default function GameRps() {
  const [myChoice, setMyChoice] = useState("");
  const [score, setScore] = useState(0);
  return (
    <Container>
      <Play setMyChoice={setMyChoice} />
    </Container>
  );
}
