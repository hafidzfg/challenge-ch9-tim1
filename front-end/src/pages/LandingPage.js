import React, { Component } from "react";
import "../style/LandingPage.css";
import "../style/Headings.css";
import {
  Container,
  Nav,
  NavbarBrand,
  NavItem,
  NavLink,
  Jumbotron,
  Button,
} from "reactstrap";
import Navbar from "../components/NavbarElement";

import "../style/LandingPage.css";
export default class LandingPage extends Component {
  render() {
    return (
      <div className="landing-page">
        <Navbar />
        <div className="pt-5 d-flex justify-content-center text-light">
          <h1>WELCOME TO OUR GAMES SITE</h1>
        </div>
        <h2 className="d-flex justify-content-center text-light">
          We provide many games for you to play
        </h2>
      </div>
    );
  }
}
