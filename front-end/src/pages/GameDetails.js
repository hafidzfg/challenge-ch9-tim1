import { Component } from "react";
import { Link } from "react-router-dom";
import React from "react";
import Navbar from "../components/NavbarElement";
import { Table, Container, Button } from "reactstrap";
import "../style/GameDetailPage.css";

class GameDetails extends Component {
  state = {
    rpsLeaderboard: [],
    gameList: [],
  };

  componentDidMount() {
    fetch("http://localhost:4000/api/v1/games/" + document.URL.substring(34))
      .then((res) => res.json())
      .then((json) => {
        this.setState({ gameList: json.data });
      });
    fetch(
      "http://localhost:4000/api/v1/games/detail?game_id=" +
        document.URL.substring(35)
    )
      .then((res) => res.json())
      .then((json) => {
        this.setState({ rpsLeaderboard: json.data });
      });
  }

  createElementGameDetail() {
    let elements = [];
    for (let i = 0; i < this.state.rpsLeaderboard.length; i++) {
      elements.push(
        <tr key={i}>
          <td className="fontTable text-center">
            {this.state.rpsLeaderboard[i].User.username}
          </td>
          <td className="fontTable text-center">
            {this.state.rpsLeaderboard[i].score}
          </td>
        </tr>
      );
    }
    return elements;
  }

  render() {
    return (
      <>
        <Navbar />
        <Container className="containerGameDetail">
          <Container className="d-flex align-items-center mb-4 contentGameDetail">
            <img
              src={this.state.gameList.thumbnail_url}
              className="imageGameSize"></img>
            <Container className="m-3">
              <p className="fontGameName">{this.state.gameList.name}</p>
              <p className="fontGameDesc">{this.state.gameList.desc}</p>
              <Link
                to={"/games/play/" + this.state.gameList.id}
                className="removeHighlight">
                <Button color="primary" size="lg">
                  Play
                </Button>
              </Link>
            </Container>
          </Container>

          <p className="h3 d-flex justify-content-center text-light">
            Leaderboard Chart
          </p>
          <Table size="large" className="leaderboardTable">
            <thead>
              <tr>
                <th className="fontTable text-center">Username</th>
                <th className="fontTable text-center">Score</th>
              </tr>
            </thead>
            <tbody>{this.createElementGameDetail()}</tbody>
          </Table>
        </Container>
      </>
    );
  }
}

export default GameDetails;
