import React, { useEffect, useState } from "react";
import "../../style/game-rps/Result.css";

const Result = ({ gameResult }) => {
  return (
    <>
      {gameResult == "" && <h3 className="text font-bold text-white">VS</h3>}
      {gameResult == "win" && (
        <div className="game__play">
          <span className="text win font-bold text-white">You Win</span>
        </div>
      )}
      {gameResult == "lose" && (
        <div className="game__play">
          <span className="text lose font-bold text-white">You Lose</span>
        </div>
      )}
      {gameResult == "draw" && (
        <div className="game__play">
          <span className="text draw font-bold text-white">Draw</span>
        </div>
      )}
    </>
  );
};

export default Result;
