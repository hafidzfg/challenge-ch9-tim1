# Challenge Chapter 9: Tim 1 Wave 22

## Members

1. Rachel Kasih Salusu
2. Hafidz Firmansyah Ghufara
3. Inggis Trisiawan
4. Wandi Irawan
5. Alviansyah Satria Maulana

## How to run

### Backend

1. `cd` ke folder `back-end/server`
2. Run `npm i`
3. Buat database di PostgreSQL dengan ubah file `config.js` dengan nama db `ch-9`, dan ubah user dan password sesuai user postgres.
4. Run `sequelize db:migrate`
5. Run `sequelize db:seed --name 20220904025924-game-list.js`
6. Run `sequelize db:seed --name 20220904043239-user.js`
7. Run `sequelize db:seed --name 20220904042918-game-detail.js`
8. Run `npm start`

### Frontend

1. `cd` ke folder `front-end`
2. Run `npm i`
3. Run `npm start`

### Endpoint

#### Backend:

GET:

- Profile: http://localhost:4000/api/v1/profile/:id
- All Users: http://localhost:4000/api/v1/users

POST:

- Register: http://localhost:4000/api/v1/register
- Login: http://localhost:4000/api/v1/login

PUT:

- Update Profile: http://localhost:4000/api/v1/profile/:id

DELETE :

- Delete User : http://localhost:4000/api/v1/users/:id

#### Frontend

- Landing Page: http://localhost:3000/
- Home Page: http://localhost:3000/
- Game List Page: http://localhost:3000/games/list
- Game Detail Page: http://localhost:3000/games/detail/:id
- Register: http://localhost:3000/register
- Login: http://localhost:3000/login
- Update Profile: http://localhost:3000/profile
- Game RPS: http://localhost:3000/games/play/:id
